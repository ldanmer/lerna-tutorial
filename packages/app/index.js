const first = require('@mylerna/first');
const second = require('@mylerna/second');

const app = () => 'Hi from the app';

const main = () => {
    console.log(app());
    console.log(first());
    console.log(second());
};

main();

module.exports = { app, main };